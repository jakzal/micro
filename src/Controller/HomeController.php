<?php

namespace Micro\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class HomeController extends Controller
{
    public function homepageAction(Request $request)
    {
        $name = $request->query->get('name', 'micro');

        if ('application/json' === $request->headers->get('Accept')) {
            $content = $this->get('jms_serializer')
                ->serialize(['name' => $name], 'json');

            return new Response(
                $content,
                200,
                ['Content-Type' => 'application/json']
            );
        }
        return $this->render(
            '@app/homepage.html.twig',
            ['name' => $name]
        );
    }
}
