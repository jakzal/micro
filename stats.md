
* symfony/symfony + the microkernel trait
  233861 vendor
  46 web

  dev Graph URL https://blackfire.io/profiles/75e9612b-19ac-4a83-8a55-24fd498842d5/graph
  Wall Time    46.6ms
  CPU Time     30.6ms
  I/O Time       16ms
  Memory       3.16MB
  
  prod Graph URL https://blackfire.io/profiles/97aad34d-22bd-4f9a-ae7d-7c35f6964f0f/graph
  Wall Time    38.5ms
  CPU Time     25.3ms
  I/O Time     13.2ms
  Memory       2.85MB

* httpkernel with event dispatcher only

  38053 vendor
  62 web
  
  Graph URL https://blackfire.io/profiles/b3ccc953-1dae-44a0-bae0-000627e7b718/graph
  Wall Time    14.1ms
  CPU Time     9.02ms
  I/O Time     5.07ms
  Memory       1.06MB
 
 
* httpkernel + router

  38316 vendor
  88 web

  Graph URL https://blackfire.io/profiles/b4fec54a-21a1-4f12-9589-cbd636f62206/graph
  Wall Time    16.7ms
  CPU Time     10.8ms
  I/O Time     5.93ms
  Memory        1.3MB

* httpkernel + stock router listener

  38316 vendor
  74 web
  
  Graph URL https://blackfire.io/profiles/b53ca3a1-ef1f-45bb-b468-5249d1e4621a/graph
  Wall Time    17.7ms
  CPU Time     11.4ms
  I/O Time     6.32ms
  Memory       1.33MB
  
* httpkernel + kernel

  58435 vendor
  97 src
  
  dev Graph URL https://blackfire.io/profiles/9f4cd10f-4974-4d17-bf5c-42d6461efe6f/graph                                                                   [44/1981]
  Wall Time    23.4ms
  CPU Time       15ms
  I/O Time     8.36ms
  Memory       1.68MB
  
  prod Graph URL https://blackfire.io/profiles/9ac78089-ea2d-4b02-8070-cd7c9ed10c1a/graph
  Wall Time    22.1ms
  CPU Time     14.1ms
  I/O Time     7.97ms
  Memory       1.65MB

* http kernel + framework bundle + microkernel trait

  101096 vendor
  53 src
   
  dev Graph URL https://blackfire.io/profiles/6a4286a5-c251-4560-bcaf-e8fa3fa15658/graph
  Wall Time    43.2ms
  CPU Time     28.3ms
  I/O Time     14.9ms
  Memory       2.65MB
  
  
  prod Graph URL https://blackfire.io/profiles/62d54e49-412c-4694-bb78-ed66633f1a03/graph
  Wall Time    35.6ms
  CPU Time     22.9ms
  I/O Time     12.8ms
  Memory       2.38MB
  
* http kernel + twig

  122491 vendor
  67 src
  
  dev Graph URL https://blackfire.io/profiles/012868f3-add4-4147-a202-c6034caacaa3/graph
  Wall Time    61.6ms
  CPU Time     40.7ms
  I/O Time     20.9ms
  Memory       3.82MB
  
  prod Graph URL https://blackfire.io/profiles/32d60ad7-1e31-406e-ba04-9d28d70bfb77/graph
  Wall Time    50.2ms
  CPU Time     32.4ms
  I/O Time     17.8ms
  Memory       3.52MB
  
  
* http kernel - with no microkernel

  138561 vendor
  60 total
  
  dev Graph URL https://blackfire.io/profiles/59bf15c9-48c2-486d-a6df-a350936d9580/graph
  Wall Time    61.2ms
  CPU Time     39.7ms
  I/O Time     21.4ms
  Memory       3.87MB
  
  prod Graph URL https://blackfire.io/profiles/8d934ab4-29ab-49cb-ad12-56c8c77aca1e/graph
  Wall Time    52.2ms
  CPU Time     33.3ms
  I/O Time     18.8ms
  Memory       3.57MB
  
* http kernel + jms serialzier

  138561 vendor 
  68 total

  dev Graph URL https://blackfire.io/profiles/12d77f9f-73e0-402c-a0a4-c6311f98105d/graph
  Wall Time    62.5ms
  CPU Time       40ms
  I/O Time     22.5ms
  Memory       3.99MB
  
  prod Graph URL https://blackfire.io/profiles/4d0f5211-ee91-4bd2-bb2d-1a9ed2c35df9/graph
  Wall Time    53.5ms
  CPU Time     34.2ms
  I/O Time     19.3ms
  Memory       3.69MB
